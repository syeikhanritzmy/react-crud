import React from 'react';
import { useState, useEffect } from 'react';
import { Form, Button, Row, Stack } from 'react-bootstrap';
function FormEdit(props) {
  const [user, setUser] = useState(props.currentUser);

  useEffect(() => {
    setUser(props.currentUser);
  }, []);
  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  };

  return (
    <>
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          props.updateUser(user.id, user);
        }}
      >
        <Row>
          <Stack gap={3}>
            <Form.Control
              name="username"
              placeholder="username"
              type="text"
              value={user.username}
              onChange={handleChangeInput}
            />
            <Form.Control
              name="password"
              placeholder="password"
              type="text"
              value={user.password}
              onChange={handleChangeInput}
            />
            <Form.Control
              name="email"
              placeholder="email"
              type="email"
              value={user.email}
              onChange={handleChangeInput}
            />

            <Button variant="primary" type="submit">
              Update
            </Button>
          </Stack>
        </Row>
      </Form>
    </>
  );
}

export default FormEdit;
